#!/bin/bash

NEEDED=(
    install/slack-desc
    usr/bin/ssh-audit
    usr/man/man1/ssh-audit.1.gz
)

for FILE in "${NEEDED[@]}"; do
    if [[ $(tar xOf "$PACKAGE_PATH" "$FILE" |wc -c) -eq 0 ]]; then
        printf 'File is missing or empty: %s\n' "$FILE"
    fi
done
