#!/bin/bash

# Project URL: https://codeberg.org/h3xx/ssh-audit.SlackBuild
#
# Copyright 2023 Dan Church <h3xx@gmx.com>
# License GPLv3: GNU GPL version 3.0 (https://www.gnu.org/licenses/gpl-3.0.html)
# This is free software: you are free to change and redistribute it.
# There is NO WARRANTY, to the extent permitted by law.

WORKDIR=${0%/*}
BASEDIR=$WORKDIR/../ssh-audit
PACKAGE_OUTPUT_DIR=$WORKDIR/..
. "$BASEDIR/ssh-audit.info" || exit
PACKAGE_NAME=$(PRINT_PACKAGE_NAME=1 bash "$BASEDIR/$PRGNAM.SlackBuild")
PACKAGE_PATH=$PACKAGE_OUTPUT_DIR/$PACKAGE_NAME

if [[ ! -f $PACKAGE_PATH ]]; then
    printf 'Building %s...\n' "$PACKAGE_PATH" >&2
    OUTPUT=$PACKAGE_OUTPUT_DIR \
    bash "$BASEDIR/$PRGNAM.SlackBuild" || exit
else
    printf 'Package %s already built.\n' "$PACKAGE_PATH" >&2
fi

printf 'Running tests...\n' >&2

export \
    DOWNLOAD \
    DOWNLOAD_x86_64 \
    EMAIL \
    HOMEPAGE \
    MAINTAINER \
    MD5SUM \
    MD5SUM_x86_64 \
    PACKAGE_NAME \
    PACKAGE_PATH \
    PRGNAM \
    REQUIRES \
    VERSION \

TESTS_RUN=0
FAILURES=()
for TEST in "$WORKDIR"/*; do
    if [[ -f $TEST && -x $TEST && ! $TEST -ef $0 ]]; then
        if OUTPUT=$("$TEST"); then
            printf '%s: PASS\n' "$TEST"
            if [[ -n $OUTPUT ]]; then
                printf -- '%s' "$OUTPUT"
            fi
        else
            FAILURES+=(
                "$TEST failed:"
                "$OUTPUT"
            )
        fi
        (( ++TESTS_RUN ))
    fi
done

if [[ $TESTS_RUN -eq 0 ]]; then
    printf 'No tests run!\n' >&2
    exit 2
fi
if [[ ${#FAILURES[@]} -gt 0 ]]; then
    for LINE in "${FAILURES[@]}"; do
        printf '%s\n' "$LINE" >&2
    done
    exit 1
else
    printf 'All tests pass!\n'
fi
