#!/bin/bash

# Project URL: https://codeberg.org/h3xx/ssh-audit.SlackBuild
#
# Copyright 2023 Dan Church <h3xx@gmx.com>
# License GPLv3: GNU GPL version 3.0 (https://www.gnu.org/licenses/gpl-3.0.html)
# This is free software: you are free to change and redistribute it.
# There is NO WARRANTY, to the extent permitted by law.

WORKDIR=${0%/*}
WORKDIR_R=$(realpath -- "$WORKDIR")

# Append :tag for a specific image version, e.g. :15.0, :current
# See https://hub.docker.com/r/vbatts/slackware/tags
IMAGE=${IMAGE:-vbatts/slackware}

if ! type docker &>/dev/null && type podman &>/dev/null; then
    # LOL
    alias docker=podman
fi

DOCKER_ARGS=(
    # Map the current directory as a volume
    -v "${WORKDIR_R//:/\\:}:/work"
    # Don't leave a dead docker container around after exiting
    --rm
)

# XXX Cannot use 'exec' here because it might be an alias
docker run "${DOCKER_ARGS[@]}" -it "$IMAGE" '/work/t/run.sh'
