#!/bin/bash

# Slackware build script for ssh-audit
#
# Project URL: https://codeberg.org/h3xx/ssh-audit.SlackBuild
#
# Copyright 2023 Dan Church <h3xx@gmx.com>
# License GPLv3: GNU GPL version 3.0 (https://www.gnu.org/licenses/gpl-3.0.html)
# This is free software: you are free to change and redistribute it.
# There is NO WARRANTY, to the extent permitted by law.

INFO_FILE=ssh-audit/ssh-audit.info

WORKDIR=${0%/*}
cd "$WORKDIR" || exit

# Clean up after ourselves
cleanup() {
    rm -fr -- "${TEMP[@]}"
}
trap 'cleanup' EXIT

# *** Install build dependencies ***
# ==================================

# TODO We should probably have our own Docker container for this, instead of
# wasting everyone's bandwidth continually re-installing these packages every
# time we run a test.

check_required_binaries() {
    local BIN MISSING=()
    for BIN; do
        if ! type -t "$BIN" &>/dev/null; then
            MISSING+=("$BIN")
        fi
    done
    if [[ ${#MISSING[@]} -gt 0 ]]; then
        printf 'Error: You are missing required programs:\n' >&2
        for BIN in "${MISSING[@]}"; do
            printf -- '- %s\n' "$BIN" >&2
        done
        exit 2
    fi
}

check_required_binaries \
    md5sum \
    slackpkg \
    wget \

# TODO slackpkg always exits code 20 with an error "No packages match the
# pattern for install" if a) already installed or b) it could not find a
# package named thus.
# Hence, we cannot exit if it fails to install, we have to check its work.
TO_INSTALL=(
    # All these are necessary for pip3 to run
    pyparsing
    python-packaging
    python-setuptools
    python-setuptools_scm
    python-tomli
)
if ! type python3 &>/dev/null; then
    TO_INSTALL+=(python3)
fi
if ! type pip3 &>/dev/null; then
    TO_INSTALL+=(python-pip)
fi

# TODO slackpkg only accepts one package argument at at time. Multiple
# arguments get ' '-glued together in a really dumb way. Technically you can
# use an egrep regex to specify package names, but this method makes this
# script simpler.
# Luckily, slackpkg will clean up after itself, so there's no need to remove
# /var/cache/packages.
for PKG in "${TO_INSTALL[@]}"; do
    yes | slackpkg install "$PKG"
done

# Check slackpkg's work
check_required_binaries \
    python3 \
    pip3 \

# Install Python dependencies
# (Evidently the version of wheel that ships in 15.0 fails to build)
pip3 install wheel

# *** Grab source code ***
# ========================

. "$INFO_FILE" || exit

# Only cleanup the source tarball if we create it
if [[ ! -f $PRGNAM/${DOWNLOAD##*/} ]]; then
    TEMP+=("$PRGNAM/${DOWNLOAD##*/}")
fi

(
cd "$PRGNAM" &&
wget --no-clobber "$DOWNLOAD" &&
printf '%s  %s\n' "${MD5SUM:-}" "${DOWNLOAD##*/}" | md5sum -c
) || exit

# *** Start build process ***
# ===========================

# Remove artifacts from running the SlackBuild script
TEMP+=(
    /tmp/SBo
    "${TMP:-}/package-$PRGNAM"
)

# XXX Cannot 'exec' here because it'll call cleanup() too early
bash "$PRGNAM/$PRGNAM.SlackBuild"
